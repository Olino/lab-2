public class Books {
    private String Title, Autor;
    private int Year;

    public Books(String Title, String Autor, int Year){
        this.Title = Title;
        this.Autor = Autor;
        this.Year = Year;
    }

    public int getYear() {
        return Year;
    }

    public String getAutor() {
        return Autor;
    }

    public String getTitle() {
        return Title;
    }

    public String toString(){
        return String.format("Название: %s\nАвтор: %s\nГод: %d\n-----\n", Title, Autor, Year);
    }
}