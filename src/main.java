import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char a;
        int b = 1, c = 0;
        int title;
        List<Books> books = new ArrayList<>();
        books.add(new Books("451° по Фаренгейту","Рей Брэдбери",1890));
        books.add(new Books("Шантарам","Грегори Дэвид Робертс",1769));
        books.add(new Books("Собачье сердце","Михаил Булгаков",1653));
        books.add(new Books("Преступление и наказание","Федор Достоевский",1888));
        books.add(new Books("Вино из одуванчиков","Рей Брэдбери",1985));
        books.add(new Books("1984","Джордж Оруэлл",1999));
        books.add(new Books("Герой нашего времени","Михаил Лермонтов",2001));
        books.add(new Books("Крестный отец","Марио Пбюзо",1999));
        while (true) {
            System.out.println("Выберите дейстиве:\n1.Поиск книги по названию\n2.Добавить книгу\n3.Удалить книгу\n4.Вывод всех книг");
            a = scanner.next().charAt(0);
            switch (a) {
                case '1': {
                    while (b != 0) {
                        System.out.print("Введите год публикации книги:");
                        title = scanner.nextInt();
                        for (int i = 0; i < books.size(); i++) {
                            if (books.get(i).getYear() == title) {
                                System.out.printf(books.get(i).toString());
                                c++;
                            }
                        }
                        if (c == 0) {
                            System.out.printf("В системе нет книги с таким годом публикации\n");
                        }
                        System.out.println("Продолжить? (д/н)");
                        b = scanner.next().charAt(0);
                        switch (b) {
                            case 'д':
                                b = 0;
                                break;
                            case 'н': {
                                b = 1;
                                break;
                            }
                        }
                    }
                    break;
                }
                case '2':{
                    String name, autor;
                    int year;
                    System.out.printf("Введите автора: ");
                    autor = scanner.next();
                    System.out.printf("Введите название книги: ");
                    name = scanner.next();
                    System.out.printf("Введите год издания: ");
                    year = scanner.nextInt();
                    books.add(new Books(name, autor, year));
                    break;
                }
                case '3':{
                    for (int i = 0; i < books.size(); i++) {
                        System.out.printf((i + 1)+ ". " + books.get(i).toString());
                    }
                    System.out.printf("Введите id книги, которую хотите удалить\n");
                    int j = scanner.nextInt();
                    books.remove(j - 1);
                    break;
                }
                case '4':{
                    for (int i = 0; i < books.size(); i++) {
                        System.out.printf((i + 1)+ ". " + books.get(i).toString());
                    }
                    break;
                }

            }
        }

    }
}
